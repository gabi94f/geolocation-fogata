<h1 align="center" id="title">Geolocation React</h1>

<p id="description">This project is designed as a take-home assignment to showcase your React skills with a primary emphasis on map integration user-friendly geolocation features and responsive design.</p>

<h2>🧐 Features</h2>

Here're some of the project's best features:

- View Existing Pins
- Add New Pin
- Pin Information Display
- Responsive Design
- Boostrap 5 Integration
- Interactive Map (Leaflet)

<h2>🛠️ Installation Steps:</h2>

<p>1. Clone the repository to your local machine</p>

```
https://gitlab.com/gabi94f/geolocation-fogata.git
```

<p>2. Install dependencies using npm</p>

```
npm install
```

<p>3. Run the development server</p>

```
npm run dev
```

<h2>💻 Built with</h2>

Technologies used in the project:

- React
- React Boostrap
- React Leaflet
