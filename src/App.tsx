import 'bootstrap/dist/css/bootstrap.min.css'
import 'leaflet/dist/leaflet.css'
import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import './App.css'
import { Map } from './components/mapComponents/map'
import AppNavbar from './components/navbar'
import Sidebar from './components/sidebar'
import { PointProvider } from './context/PointContext'

const App: React.FC = () => {
  return (
    <PointProvider>
      <div className='app-container dark-mode'>
        <AppNavbar />
        <Container fluid className='app-content'>
          <Row>
            <Col md={6}>
              <Map />
            </Col>
            <Col md={6}>
              <Sidebar />
            </Col>
          </Row>
        </Container>
      </div>
    </PointProvider>
  )
}

export default App
