import 'leaflet/dist/leaflet.css'
import { useState } from 'react'
import { Button, Container, Form, Modal } from 'react-bootstrap'
import { MapContainer as LeafMap, TileLayer, useMapEvents } from 'react-leaflet'
import PointMarker from '../pointMarker'
import { Point } from '../../../interfaces'
import { usePointContext } from '../../../context/usePointContext'

export const Map = () => {
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false)
  const [clickCoordinates, setClickCoordinates] = useState<number[] | null>(
    null
  )
  const [description, setDescription] = useState<string>('')
  const [error, setError] = useState<string>('')

  const { points, addPoint } = usePointContext()

  const MapContainer = () => {
    useMapEvents({
      click: e => {
        const { lat, lng } = e.latlng
        setClickCoordinates([lng, lat])
        setModalIsOpen(true)
      }
    })
    return null
  }

  const handleFormSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if (description.trim() === '') {
      setError('Description is required')
      return
    }

    const newPoint = {
      type: 'Feature',
      properties: {
        description: description
      },
      geometry: {
        type: 'Point',
        coordinates: clickCoordinates || []
      }
    }

    addPoint(newPoint)
    setDescription('')
    setModalIsOpen(false)
  }

  const hideModal = () => {
    setModalIsOpen(false)
    setDescription('')
    setError('')
  }

  return (
    points && (
      <Container>
        <p className='title'>Click on the map to add a point</p>
        <LeafMap
          center={[
            points[0].geometry.coordinates[1],
            points[0].geometry.coordinates[0]
          ]}
          zoom={13}
          scrollWheelZoom
          style={{ height: '500px' }}
        >
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />
          <MapContainer />
          {points.map((point: Point, index: number) => (
            <PointMarker key={index} point={point} />
          ))}
        </LeafMap>
        <Modal show={modalIsOpen} onHide={hideModal} centered>
          <Modal.Header closeButton>
            <Modal.Title>Crear punto</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleFormSubmit}>
              <Form.Group className='mb-3' controlId='description'>
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type='text'
                  placeholder='Enter description'
                  value={description}
                  onChange={e => setDescription(e.target.value)}
                  isInvalid={error !== ''}
                  color='red'
                />
                <Form.Text className='text-muted'>{error}</Form.Text>
              </Form.Group>
              <Button type='submit' disabled={description.trim() === ''}>
                Add point
              </Button>
            </Form>
          </Modal.Body>
        </Modal>
      </Container>
    )
  )
}
