import { FC } from 'react'
import { Marker, Popup } from 'react-leaflet'
import { usePointContext } from '../../../context/usePointContext'
import { PointMarkerProps } from '../../../interfaces'

const PointMarker: FC<PointMarkerProps> = ({ point }) => {
  const { saveDetailPoint } = usePointContext()

  const handleClick = () => {
    saveDetailPoint(point)
  }

  return (
    <Marker
      position={[point.geometry.coordinates[1], point.geometry.coordinates[0]]}
      eventHandlers={{ click: handleClick }}
    >
      <Popup>{point.properties.description}</Popup>
    </Marker>
  )
}

export default PointMarker
