import React from 'react'
import Container from 'react-bootstrap/Container'
import Navbar from 'react-bootstrap/Navbar'

const AppNavbar: React.FC = () => {
  return (
    <Navbar className='navbar' data-bs-theme='dark'>
      <Container fluid>
        <Navbar.Brand href='#home'>Geolocation Fogata</Navbar.Brand>
        <Navbar.Toggle aria-controls='basic-navbar-nav' />
      </Container>
    </Navbar>
  )
}

export default AppNavbar
