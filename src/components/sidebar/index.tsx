import { FC } from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { usePointContext } from '../../context/usePointContext'

const Sidebar: FC = () => {
  const { detailPoint } = usePointContext()
  const { description } = detailPoint?.properties || {}
  const { coordinates } = detailPoint?.geometry || {}

  return (
    <div>
      <p className='title'>Points details</p>

      <Container className='sidebar-container' fluid>
        <Row>
          {detailPoint ? (
            <Col>
              <p className='h6'>Latitude: {coordinates && coordinates[0]}</p>
              <p className='h6'>Longitude: {coordinates && coordinates[1]}</p>
              <p className='h6'>Description: {description}</p>
            </Col>
          ) : (
            <p className='h6'>Select a point on the map to see detail</p>
          )}
        </Row>
      </Container>
    </div>
  )
}

export default Sidebar
