import React, { ReactNode, createContext, useEffect, useState } from 'react'
import { Point } from '../interfaces'
import data from '../utils/points.json'

export interface PointContextType {
  points: Point[] | undefined
  saveDetailPoint: (point: Point) => void
  detailPoint: Point | null
  addPoint: (point: Point) => void
}

export const PointContext = createContext<PointContextType | undefined>(
  undefined
)

interface PointProviderProps {
  children: ReactNode
}

export const PointProvider: React.FC<PointProviderProps> = ({ children }) => {
  const [points, setPoints] = useState<Point[]>()
  const [detailPoint, setDetailPoint] = useState<Point | null>(null)

  useEffect(() => {
    setPoints(data.features)
  }, [])

  const saveDetailPoint = (point: Point) => {
    setDetailPoint(point)
  }

  const addPoint = (point: Point) => {
    setPoints(points ? [...points, point] : [point])
  }

  return (
    <PointContext.Provider
      value={{
        points,
        saveDetailPoint,
        detailPoint,
        addPoint
      }}
    >
      {children}
    </PointContext.Provider>
  )
}
