import { useContext } from 'react'
import { PointContext, PointContextType } from './PointContext'

export function usePointContext (): PointContextType {
  const context = useContext(PointContext)

  if (!context) {
    throw new Error('Used ModalContext outside of ModalProvider')
  }

  return context
}
