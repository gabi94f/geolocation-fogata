export interface Point {
  type: string
  properties: {
    description: string
  }
  geometry: {
    type: string
    coordinates: number[]
  }
}

export interface PointMarkerProps {
  point: Point
}
